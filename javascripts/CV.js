$("#add_circle_to_canvas_button").on("click", function(e){
	CV.addACircleToCanvas($("#my_canvas")[0], 30);
});

$("#add_rect_to_canvas_button").on("click", function(e){
	CV.addARectangleToCanvas($("#my_canvas")[0], 50, 100);
});

$("#reset_canvas_button").on("click", function(e){
	CV.resetCanvas($("#my_canvas")[0]);
});

$("#snap_window_button").on("click", function(e){
	CV.snapWindow($("#my_canvas")[0]);
});

$("#translate_and_rotate_canvas_button").on("click", function(e){
	CV.translateAndRotateCanvas($("#my_canvas")[0]);
});

var CV = (function(){
	function _release_public(){
		return {
			init: init,
			addACircleToCanvas: addACircleToCanvas,
			addARectangleToCanvas: addARectangleToCanvas,
			resetCanvas: resetCanvas,
			snapWindow: snapWindow,
			translateAndRotateCanvas: translateAndRotateCanvas
		}
	};
	
	/*----------private-----------*/
	/**
	* _Get RGBA
	* 
	* @param Float opacity
	* 
	* @return String rgba
	*/
	function _getRGBA(opacity){
		var rand1 = Math.floor(Math.random()*256),
			rand2 = Math.floor(Math.random()*256),
			rand3 = Math.floor(Math.random()*256),
			rand = [rand1, rand2, rand3].toString();
		//console.log("CV::_getRGBA: " + rand);
		return "rgba(" + rand+ "," + opacity + ")";
	}
	
	
	/*
		@TODO
		TWO BELOW ARE TRIPLE CODE REFACTOR
		Terrible passing canvas height and width use defaults
		
	*/
	
	/**
	* _Get Position X
	* 
	* @param Integer max_widthI
	* 
	* @return integer x
	*/
	function _getPositionX(max_widthI){
		var	x = Math.floor(Math.random()*max_widthI);
		//console.log("CV::_getPositionX: " + x);
		return x;
	}
	
	/**
	* _Get Position Y
	* 
	* @param Integer max_heightI
	* 
	* @return integer y
	*/
	function _getPositionY(max_heightI){
		var y = Math.floor(Math.random()*max_heightI);
		//console.log("CV::_getPositionY: " + y);
		return y;
	}
	
	/**
	* _Get Radius
	* 
	* @param Integer max_radius
	* 
	* @return integer radius
	*/
	function _getRadius(max_radiusI){
		var	r = Math.floor(Math.random()*max_radiusI);
		//console.log("CV::_getRadius: " + r);
		return r;
	}
	
	
	
	
	
	/*----------public------------*/
	
	/**
	 * Add A Circle To Canvas
	 * 
	 * @param DOMElement canvas
	 * @param Integer max_radius
	 * @return null
	 */
	function addACircleToCanvas(canvasElement, max_radius){
		var radius = _getRadius(max_radius);
		//console.log("CV::AddACircleToCanvas: With a radius of " + radius);
		var ctx = canvasElement.getContext("2d");
		ctx.beginPath();
		ctx.fillStyle = _getRGBA(0.7); 
		ctx.arc(_getPositionX(canvasElement.width), _getPositionY(canvasElement.height), radius, 0, Math.PI*2, true); 
		ctx.closePath();
		ctx.fill();
		return null;
	};
	
	/**
	 * Add A Rectangle To Canvas
	 * 
	 * @param DOMElement canvas
	 * @param Integer widthI
	 * @param Integer heightI
	 * 
	 * @return null
	 */
	function addARectangleToCanvas(canvasElement, widthI, heightI){
		//console.log("CV::AddACircleToCanvas: With a width of: " + widthI + " and a height of: " + heightI);
		var ctx = canvasElement.getContext("2d");
		//ctx.beginPath();
		ctx.fillStyle = _getRGBA(0.7);
		//console.log(ctx.fillStyle);
		ctx.fillRect(_getPositionX(canvasElement.width), _getPositionY(canvasElement.height), widthI, heightI);  
		//ctx.closePath();
		//ctx.fill();
		return null;
	};
	
	
	/**
	 * Reset Canvas
	 * 
	 * Using a width adjustment clearing method
	 * Try this with translateAndRotateCanvas and you will see canvas is reset in terms of rotaton etc
	 * 
	 * @param DOMElement canvas
	 * 
	 * @return null
	 */
	function resetCanvas(canvasElement){
		var ctx = canvasElement.getContext("2d");
		console.log("CV::resetCanvas: Sigh...You would think there would be a better reset retransform rerotate clear canvas method built in");
		canvasElement.width++;
		canvasElement.width--;
		return null;
	};
	
	/**
	 * Snap Window
	 * 
	 * @param DOMElement canvas
	 * 
	 * @return null
	 */
	function snapWindow(canvasElement){
		var ctx = canvasElement.getContext("2d");
		ctx.drawWindow(window, 0, 0, 100, 200, "rgb(255,255,255)");  
		console.log("CV::snapWindow: requires the Chrome flags");
		return null;
	};
	
	/**
	 * Translate And Rotate Canvas
	 * Multi steps the translation and rotation of the Canvas using
	 * save and restore
	 * For clarity I do not use the addARectangleToCanvas function
	 * as it creates a new context
	 *
	 * Try commenting out the pairs of ctx.saves and restores in various compbinations
	 * to get an idea of what happens a) every time this function is called
	 * and b) actions (steps) inside this function
	 * 
	 * @param DOMElement canvas
	 * 
	 * @return null
	 */
	function translateAndRotateCanvas(canvasElement){
		var ctx = canvasElement.getContext("2d");
		
		ctx.save();
			ctx.rotate(15 * Math.PI / 180);
			ctx.fillStyle = _getRGBA(0.7);
			ctx.fillRect(120, 12, 30, 30);
		ctx.restore();  
  
		//ctx.save();
			ctx.rotate(15 * Math.PI / 180);
			ctx.fillStyle = _getRGBA(0.7);
			ctx.fillRect(120, 12, 30, 30);
		//ctx.restore();  
		
		ctx.save(); //point/state A
			ctx.rotate(15 * Math.PI / 180);
			ctx.fillStyle = _getRGBA(0.7);
			ctx.fillRect(120, 12, 30, 30);
		ctx.restore(); 
		
		//any other function called asfter this will be using point/state A
		
		//console.log("CV::translateAndRotateCanvas: completed")
	}
	

	
	function init(){
		//	console.log("CV::init: Good to go!");	
	};
		
	return _release_public();
})();



	


